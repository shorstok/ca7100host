#pragma once
#include <winsock2.h>
#include <deque>
#include "Ca7100HostCommand.h"
#include "Schema.h"
#include <vector>

class CHostConnection
{
private:

	SOCKET m_socket = INVALID_SOCKET;
	std::deque<char> stream;
	int tagCounter = 1;


public:

	int hostPort = 8841;
	std::string hostIp = "127.0.0.1";

public:

	void cleanup() const;

	void initialize_and_connect();

	CCa7100HostCommand send_request(CCa7100HostCommand request);

	bool sendline(std::string line) const;

	std::string readline();

	void set_async_error_handler(std::function<void(std::string)> onError);
	void set_async_notification_handler(std::function<void(std::string, std::string)> onNotification);

	std::map<std::string,std::string> state_variables;

private:

	void handle_oob(CCa7100HostCommand& ca7100_host_command);
	std::function<void(std::string)> _oob_error_handler = nullptr;
	std::function<void(std::string, std::string)> _oob_notification_handler = nullptr;

};
