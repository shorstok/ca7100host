#pragma once
#include <sstream>
#include <map>
#include <functional>
#include "Schema.h"

class CCa7100HostCommand
{
public:

	std::string command;
	std::map<std::string, std::string> arguments;

	bool is_empty() const { return command.empty(); }
	bool is_error() const { return command == schema::commands::error::name;}
	bool is_success() const { return command == schema::commands::response::name; }

	int get_error_code() const
	{
		const auto code = arguments.find(schema::commands::error::arguments::code);

		if(code == arguments.end())
			return schema::commands::error::codes::success;

		return std::stoi(code->second);
	}

public:

	CCa7100HostCommand();

	explicit CCa7100HostCommand(const std::string& rawData);

	CCa7100HostCommand(const std::string& command, std::initializer_list<std::pair<std::string const, std::string>> arguments);

	CCa7100HostCommand& on_ok(std::function<void(CCa7100HostCommand&)> onOk);
	CCa7100HostCommand& on_error(std::function<void(CCa7100HostCommand&)> onError);

	std::string pack() const;
};
