#include "stdafx.h"
#include "Ca7100HostCommand.h"
#include "Parser.h"
#include "Schema.h"
#include <functional>

CCa7100HostCommand::CCa7100HostCommand()
{
}

CCa7100HostCommand::CCa7100HostCommand(const std::string& raw_data)
{
	if (raw_data.empty())
		throw std::runtime_error("Empty command");

	auto space_pos = raw_data.find_first_of(' ');

	if (space_pos == std::string::npos)
	{
		command = raw_data;
		return;
	}

	command = raw_data.substr(0, space_pos);
	auto args_raw = raw_data.substr(space_pos + 1, raw_data.length() - space_pos);

	if (args_raw.length() > 0)
	{
		auto argsSource = Parser::split(args_raw, ',');

		for (auto& arg : argsSource)
		{
			auto tuple = Parser::split(arg, '=');

			if (tuple.size() > 1)
				arguments[Parser::unescape(tuple[0])] = Parser::unescape(tuple[1]);
			else
				arguments[Parser::unescape(tuple[0])] = std::string();
		}
	}
}


CCa7100HostCommand::CCa7100HostCommand(const std::string& command, std::initializer_list<std::pair<std::string const, std::string>> arguments):
	command(command),
	arguments(arguments)
{
}

CCa7100HostCommand& CCa7100HostCommand::on_ok(std::function<void(CCa7100HostCommand&)> handler)
{
	if (is_success())
		handler(*this);

	return *this;
}

CCa7100HostCommand& CCa7100HostCommand::on_error(std::function<void(CCa7100HostCommand&)> handler)
{
	if (is_error())
		handler(*this);

	return *this;
}


std::string CCa7100HostCommand::pack() const
{
	std::stringstream stream;

	stream << command;

	if (!arguments.empty())
		stream << " ";

	bool firstArgument = true;

	for (auto& arg : arguments)
	{
		if(!firstArgument)
			stream<<",";

		firstArgument = false;

		stream << Parser::escape(arg.first);

		if (arg.second.length() < 1)
			continue;

		stream << "=" << Parser::escape(arg.second);
	}

	stream << "\n";

	return stream.str();
}
