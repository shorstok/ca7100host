#pragma once
#include <string>
#include <map>
#include <vector>
#include <sstream>

class Parser
{
public:

	static void trim(std::string& s);
	
	static std::string escape(std::string source);
	static std::string unescape(std::string source);

	static std::vector<std::string> split(const std::string& s, char delim);

private:

	static class _init
	{
	public:
		_init();
	} _initializer;
	
	const static char screenStartDelimiter = '&';
	const static char screenEndDelimiter = ';';

	static std::string screenDelimiters;
	static std::vector<std::pair<char, std::string>> screenedCharacters;

	static std::map<char, std::string> screenLookup;
	static std::map<std::string, char> screenReverseLookup;

	// Disallow creating an instance of this object
	Parser();
};
