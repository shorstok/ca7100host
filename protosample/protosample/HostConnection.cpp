#include "stdafx.h"

#define _WINSOCK_DEPRECATED_NO_WARNINGS 1		//to circumvent microsoft dependency

#include "HostConnection.h"
#include <string>
#include <winsock2.h>
#include <deque>
#include "Parser.h"
#include <iostream>


//Include winsock library
#pragma comment (lib, "Ws2_32.lib")


void CHostConnection::initialize_and_connect()
{
	WSADATA wsa_data;
	sockaddr_in addr;

	m_socket = INVALID_SOCKET;

	auto error_code = WSAStartup(MAKEWORD(2, 2), &wsa_data);

	if (error_code != 0)
		throw std::runtime_error("WSAStartup failed");

	ZeroMemory(&addr, sizeof addr);

	addr.sin_family = AF_INET;
	addr.sin_port = htons(hostPort);
	addr.sin_addr.s_addr = inet_addr(hostIp.c_str());

	m_socket = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);

	if (m_socket == INVALID_SOCKET)
	{
		error_code = WSAGetLastError();//todo : something with error_code
		throw std::runtime_error("Socket creation failed");
	}

	if (connect(m_socket, reinterpret_cast<const sockaddr*>(&addr), sizeof addr) != 0)
	{
		error_code = WSAGetLastError(); //todo : something with error_code
		throw std::runtime_error("Connect failed");
	}
}

CCa7100HostCommand CHostConnection::send_request(CCa7100HostCommand request)
{
	CCa7100HostCommand response;
	auto requestTag = std::to_string(++tagCounter);

	//Apply tag to identify response to our request
	request.arguments[schema::commands::shared_arguments::tag] = requestTag;

	if (!sendline(request.pack()))
		throw std::runtime_error("sendline failed");

	do
	{
		//Means we have response from previous loop that didn't pass 'tag' matching -
		//probably status update or some other OOB info from host

		if (!response.is_empty())
			handle_oob(response);

		auto responseLine = readline();

		if (responseLine.empty())
			throw std::runtime_error("no response!");

		Parser::trim(responseLine);

		response = CCa7100HostCommand(responseLine);

		//If we get 'syntax error', command tag may be unparsed, so
		//response.arguments[schema::argument::tag] == requestTag would never be satisfied,
		//so throw on any syntax error

		response.on_error([](CCa7100HostCommand error)
			{
				if (error.get_error_code() == schema::commands::error::codes::syntax)
					throw std::runtime_error(std::string("Syntax error in request: ") + error.arguments[schema::commands::error::arguments::text]);
			});
	}
	while (response.arguments[schema::commands::shared_arguments::tag] != requestTag);

	return response;
}


void CHostConnection::handle_oob(CCa7100HostCommand& command)
{
	//
	if(command.is_error())
	{
		const auto text = command.arguments.find(schema::commands::error::arguments::text);

		if(text != command.arguments.end() && _oob_error_handler!=nullptr)
			_oob_error_handler(text->second);
	}
	else if(command.command == schema::commands::notification::name)
	{
		auto type = command.arguments.find(schema::commands::notification::arguments::notification_type);
		
		if(type == command.arguments.end())
			return;

		if(type->second == schema::commands::notification::status_reserved_token)
		{
			state_variables[command.arguments[schema::commands::notification::arguments::status_changed_source_name]] = 
				command.arguments[schema::commands::notification::arguments::payload];
		}
		else if(_oob_notification_handler!=nullptr)
		{
			_oob_notification_handler(command.arguments[schema::commands::notification::arguments::notification_type],
				command.arguments[schema::commands::notification::arguments::payload]);
		}
			
	}
}


bool CHostConnection::sendline(std::string line) const
{
	std::vector<char> sendbuf(line.begin(), line.end());

	while (!sendbuf.empty())
	{
		//Try to send at most all data
		auto bytesSent = send(m_socket, sendbuf.data(), sendbuf.size(), 0);

		//disconnect
		if (bytesSent == 0)
			return false;

		if (bytesSent < 0)
			throw std::runtime_error("Socket send failed");

		//Remove data sent from sendbuf and try sending rest if something left
		sendbuf.erase(sendbuf.begin(), sendbuf.begin() + bytesSent);
	}

	return true;
}

std::string CHostConnection::readline()
{
	const int buf_len = 128;
	char buf[buf_len];
	int line_end_pos = -1;

	while (true)
	{
		auto bytes_got = recv(m_socket, buf, buf_len, 0);

		//Disconnect
		if (bytes_got == 0)
			break;

		//error
		if (bytes_got < 0)
			throw std::runtime_error("Socket recv failed");

		//Actully got data

		for (int c = 0; c < bytes_got; ++c)
		{
			stream.push_back(buf[c]);

			if (buf[c] == '\n' && line_end_pos < 0)
				line_end_pos = stream.size();
		}

		if (line_end_pos > 0)
			break;
	}

	if (line_end_pos < 1)
		return std::string();

	auto result = std::string(stream.begin(), stream.begin() + line_end_pos);

	stream.erase(stream.begin(), stream.begin() + line_end_pos);

	return result;
}

void CHostConnection::set_async_error_handler(std::function<void(std::string)> onError)
{
	_oob_error_handler = onError;
}

void CHostConnection::set_async_notification_handler(std::function<void(std::string, std::string)> onNotification)
{
	_oob_notification_handler = onNotification;
}

void CHostConnection::cleanup() const
{
	auto error_code = shutdown(m_socket, SD_SEND);

	if (error_code == SOCKET_ERROR)
	{
		closesocket(m_socket);
		WSACleanup();
		throw std::runtime_error("shutdown failed");
	}

	if (m_socket != INVALID_SOCKET)
		closesocket(m_socket);

	WSACleanup();
}
