#include "stdafx.h"

#include <winsock2.h>
#include <cstdlib>
#include <cstdio>
#include <mutex>
#include <map>
#include <sstream>
#include <iterator>
#include <iostream>

#include "Parser.h"
#include <deque>
#include "Schema.h"
#include "Ca7100HostCommand.h"
#include "HostConnection.h"

void defaultActionOnError(CCa7100HostCommand& cmd)
{
	std::cout << cmd.arguments[schema::commands::response::arguments::command_name] << " - got error: ";
	std::cout << "code: " << cmd.arguments[schema::commands::error::arguments::code] << ", ";
	std::cout << "error text: `" << cmd.arguments[schema::commands::error::arguments::text] << "`\n";
};

void log_async_error(std::string text)
{
	std::cout << "Get error during execution : " << text << std::endl;
}

void log_async_notification(std::string source, std::string payload)
{
	std::cout << "Host notification (" << source << "): " << payload << std::endl;
}


void defaultActionOnFatalError(CCa7100HostCommand& cmd)
{
	std::cout << std::string(10, '*') << "\n";
	std::cout << "Error - execution cannot continue!\n";
	std::cout << "Error code: " << cmd.arguments[schema::commands::error::arguments::code] << "\n";
	std::cout << "Error text: " << cmd.arguments[schema::commands::error::arguments::text] << "\n";
	std::cout << std::string(10, '*') << "\n";

	throw std::runtime_error("Cannot continue");
};

std::string format(const std::string fmt_str, ...)
{
	int n = int(fmt_str.size()) * 2; /* Reserve two times as much as the length of the fmt_str */
	std::unique_ptr<char[]> formatted;
	va_list ap;
	while (true)
	{
		formatted.reset(new char[n]); /* Wrap the plain char array into the unique_ptr */
		strcpy_s(&formatted[0], n, fmt_str.c_str());
		va_start(ap, fmt_str);
		const int final_n = vsnprintf(&formatted[0], n, fmt_str.c_str(), ap);
		va_end(ap);
		if (final_n < 0 || final_n >= n)
			n += abs(final_n - n + 1);
		else
			break;
	}
	return std::string(formatted.get());
}

int main()
{
	CHostConnection connection;

	try
	{
		connection.set_async_error_handler(log_async_error);
		connection.set_async_notification_handler(log_async_notification);

		connection.initialize_and_connect();

		//First line from host is always some 'hello message'
		std::cout << "Hail message: " << connection.readline();

		//First get info about host we're connected to

		std::cout << "Getting system info..." << std::endl;

		connection.send_request(CCa7100HostCommand(schema::commands::info::name, {/* command takes no arguments */})).
		           on_error(defaultActionOnError).
		           on_ok([](CCa7100HostCommand& cmd)
		           {
			           std::cout << "host core version: " << cmd.arguments[schema::commands::info::coreversion] << "\n";
			           std::cout << "host version: " << cmd.arguments[schema::commands::info::hostversion] << "\n";
		           });


		//Switch to EN localization

		std::cout << "Switching to EN..." << std::endl;

		connection.send_request(CCa7100HostCommand(schema::commands::localization::name, {
			{schema::commands::localization::arguments::language, "en"}
		})).on_error(defaultActionOnFatalError);

		//Get config

		std::cout << "Getting config..." << std::endl;

		connection.send_request(CCa7100HostCommand(schema::commands::config::name, {{
			schema::commands::config::action,
			schema::commands::config::arguments::list_argument}})).
		           on_error(defaultActionOnError).
		           on_ok([](CCa7100HostCommand& cmd)
		           {
						std::cout << "Current config" << std::endl;

						for(auto & item : cmd.arguments)
							std::cout << item.first << " = " << item.second << std::endl;
		           });

		//Get current measurement system state

		std::cout << "Getting current measurement system state..." << std::endl;

		connection.send_request(CCa7100HostCommand(schema::commands::state::name, {/* command takes no arguments */})).
		           on_error(defaultActionOnError).
		           on_ok([](CCa7100HostCommand& cmd)
		           {
			           std::cout << "Current state: `" << cmd.arguments[schema::commands::state::arguments::current] << "`\n";
		           });

		//Attempt non-existent command - should be 'error'

		std::cout << "Testing for invalid command 'fake_command'..." << std::endl;

		connection.send_request(CCa7100HostCommand("fake_command", {})).
		           on_error(defaultActionOnError).
		           on_ok([](CCa7100HostCommand& cmd)
		           {
			           throw std::runtime_error("Should never get here");
		           });

		//Power on measurement unit

		std::cout << "Powering on measurement unit..." << std::endl;

		auto response = connection.send_request(CCa7100HostCommand(schema::commands::power::name,
		                                                           {{schema::commands::power::arguments::turn_on, ""}})).
		                           on_ok([](CCa7100HostCommand& cmd)
		                           {
			                           std::cout << "Power on OK, reply code: `" << cmd.arguments[schema::commands::error::
					                           arguments::code] <<
				                           "`\n";
		                           });

		if (response.is_error())
		{
			if (response.get_error_code() == schema::commands::error::codes::coeffs_not_found)
			{
				//coeffs for device not found - load them from Control Block
				//another option is to load from file, but it's not shown in this example

				std::cout << "Calibration Coefficients not found for device  #`" << response.arguments[schema::commands::error::
					arguments::text] << std::endl;
				std::cout << "Please, connect Control Block  #" << response.arguments[schema::commands::error::arguments::text] <<
					std::endl;

				//Ask Ca7100 Host to load coefficients from control block
				connection.send_request(CCa7100HostCommand(schema::commands::coefficients::name, {
					           {
						           schema::commands::coefficients::arguments::source,
						           schema::commands::coefficients::arguments::source_control_block_token
					           }
				           })).
				           on_error(defaultActionOnFatalError).
				           on_ok([](CCa7100HostCommand& cmd) { std::cout << "Coeffs import OK, retrying power-on" << std::endl; });

				//Power on once more
				connection.send_request(CCa7100HostCommand(schema::commands::power::name,
				                                           {{schema::commands::power::arguments::turn_on, ""}})).
				           on_error(defaultActionOnFatalError).
				           on_ok([](CCa7100HostCommand& cmd)
				           {
					           std::cout << "Power on OK, reply code: `" << cmd.arguments[schema::commands::error::arguments::code] <<
						           "`\n";
				           });
			}
			else
			{
				std::cout << "Power on FAILED, reply code: `" << response.get_error_code() << std::endl;
				return -1;
			}
		}

		//Now ensure test device is connected by repeatedly trying to set base cx testing configuration

		std::cout << "Looking for testing device..." << std::endl;

		while (connection.send_request(CCa7100HostCommand(schema::commands::test_device::name,
		                                                  {
			                                                  {
				                                                  schema::commands::test_device::configuration_argument,
				                                                  schema::commands::test_device::test_configuration_cx
			                                                  },
			                                                  {schema::commands::test_device::relay_argument, "1"},
		                                                  })).
		                  get_error_code() == schema::commands::error::codes::test_device_not_connected)
		{
			std::cout << "Please, connect test device and press ENTER..." << std::endl;
			std::cin.get();
		}

		//Run test with all test device combinations

		for (int auxcomb = 0; auxcomb < 2; auxcomb++)
		{
			for (int basecomb = 1; basecomb < 9; ++basecomb)
			{
				std::cout << ">> Test CX [" << basecomb << ":" << auxcomb << "]:\t";

				auto testconfig = connection.send_request(CCa7100HostCommand(schema::commands::test_device::name,
				                                                             {
					                                                             {
						                                                             schema::commands::test_device::
						                                                             configuration_argument,
						                                                             schema::commands::test_device::test_configuration_cx
					                                                             },
					                                                             {
						                                                             schema::commands::test_device::relay_argument,
						                                                             std::to_string(basecomb)
					                                                             },
					                                                             {
						                                                             schema::commands::test_device::aux_relay_argument,
						                                                             std::to_string(auxcomb)
					                                                             },
				                                                             })).on_error(defaultActionOnFatalError);

				auto tgmin = std::stod(testconfig.arguments[schema::commands::test_device::result_tgmin]);
				auto tgmax = std::stod(testconfig.arguments[schema::commands::test_device::result_tgmax]);
				auto cmin = std::stod(testconfig.arguments[schema::commands::test_device::result_cmin]);
				auto cmax = std::stod(testconfig.arguments[schema::commands::test_device::result_cmax]);

				std::cout << format("cx: (%.3e F ... %.3e F)\t", cmin, cmax);
				std::cout << format("tg: (%.3e ... %.3e), measuring...", tgmin, tgmax);
				std::cout << std::endl;

				//Measure using reference C0 value 1nF (test device reference) and 3 takes averaging

				auto measurement = connection.send_request(CCa7100HostCommand(schema::commands::measure::name,
				                                                              {
					                                                              {
						                                                              schema::commands::measure::arguments::what,
						                                                              schema::commands::measure::cx::key
					                                                              },
					                                                              {schema::commands::measure::cx::c0_value, "1e-9"},
					                                                              {schema::commands::measure::cx::tg0_value, "0"},
					                                                              {schema::commands::measure::cx::cx_averaging, "3"}
				                                                              })).on_error(defaultActionOnFatalError);


				auto cx = std::stod(measurement.arguments[schema::commands::measure::cx::resulting_capacity]);
				auto tgx = std::stod(measurement.arguments[schema::commands::measure::cx::resulting_tangent]);

				std::cout << "Result: ";
				std::cout << format("cx: %.3e F\t", cx);
				std::cout << format("tgx: %.3e: ", tgx);

				//Now just compare -- whether measured cx and tangent are in reference limits provided by testing device

				if (cx >= cmin && cx <= cmax && tgx >= tgmin && tgx <= tgmax)
					std::cout << "SUCCESS";
				else
					std::cout << "FAILURE!";

				std::cout << std::endl;
			}
		}

		//Power off measurement unit

		std::cout << "Powering off measurement unit..." << std::endl;

		connection.send_request(CCa7100HostCommand(schema::commands::power::name,
		                                           {{schema::commands::power::arguments::turn_off, ""}})).
		           on_error(defaultActionOnFatalError);


		std::cout << "Disconnecting..." << std::endl;

		//Disconnect
		connection.cleanup();
	}
	catch (const std::runtime_error& e)
	{
		std::cout << "Execution stopped due to runtime_error exception:\n";
		std::cout << e.what() << "\n";
	}


	std::cout << "Press ENTER to leave\n";
	std::cin.get();

	return 0;
}
