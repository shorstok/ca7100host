#include "stdafx.h"
#include "Parser.h"
#include <iterator>
#include <string>
#include <locale>
#include <algorithm>

//Create static stuff
std::string Parser::screenDelimiters;
std::vector<std::pair<char, std::string>> Parser::screenedCharacters;

std::map<char, std::string> Parser::screenLookup;
std::map<std::string, char> Parser::screenReverseLookup;

//Run static initialization
Parser::_init Parser::_initializer;


std::vector<std::string> Parser::split(const std::string& s, char delim)
{
	std::vector<std::string> elems;
	std::stringstream ss(s);
	std::string item;

	auto result = std::back_inserter(elems);

	while (std::getline(ss, item, delim))
	{
		*(result++) = item;
	}

	return elems;
}


// trim from start (in place)
static inline void ltrim(std::string& s)
{
}

// trim from end (in place)
static inline void rtrim(std::string& s)
{
}

// trim from both ends (in place)
void Parser::trim(std::string& str)
{
	size_t endpos = str.find_last_not_of(" \r\n\t");
	size_t startpos = str.find_first_not_of(" \r\n\t");

	if (std::string::npos != endpos)
	{
		str = str.substr(0, endpos + 1);
		str = str.substr(startpos);
	}
	else
	{
		str.erase(std::remove(std::begin(str), std::end(str), ' '), std::end(str));
	}
}

std::string Parser::escape(std::string source)
{
	std::stringstream result;

	for (const auto& ch : source)
	{
		auto subst = screenLookup.find(ch);

		if (subst != screenLookup.end())
		{
			result << screenStartDelimiter << subst->second << screenEndDelimiter;
			continue;
		}

		result << ch;
	}

	return result.str();
}

std::string Parser::unescape(std::string source)
{
	std::stringstream result;

	for (int i = 0; i < source.length(); i++)
	{
		if (source[i] != screenStartDelimiter)
		{
			result << source[i];
			continue;
		}

		auto end = source.find_first_of(screenDelimiters, i + 1);

		if (end != std::string::npos && end > i + 1 && source[end] == screenEndDelimiter)
		{
			auto entity = source.substr(i + 1, end - i - 1);

			i = end;


			auto replacement = screenReverseLookup.find(entity.c_str());

			if (replacement != screenReverseLookup.end())
			{
				result << replacement->second;
				continue;
			}

			result << screenStartDelimiter << entity << screenEndDelimiter;
		}
		else
			result << source[i];
	}

	return result.str();
}

Parser::_init::_init()
{
	screenedCharacters = {
		{'"', "dq"},
		{'`', "bq"},
		{'\'', "q"},
		{',', "c"},
		{'&', "a"},
		{'\n', "n"},
		{'\r', "r"},
		{'=', "e"}
	};

	screenDelimiters = std::string({screenStartDelimiter ,screenEndDelimiter});

	for (auto const& token : screenedCharacters)
	{
		screenLookup[token.first] = token.second;
		screenReverseLookup[token.second] = token.first;
	}
}

Parser::Parser()
{
}
