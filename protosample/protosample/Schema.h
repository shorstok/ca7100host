#pragma once

namespace schema
{
	namespace commands
	{
		/// <summary>
		/// Arguments used in more than one command
		/// </summary>
		namespace shared_arguments
		{
			/// <summary>
			/// Field used to identify command. 
			/// It's carried along in all responses (error & successful), used
			/// to cancel command with <see cref="Cancel"/>
			/// 
			/// Client duty is to make sure it's unique and doesnt repeat
			/// </summary>
			const char* const tag = "tag";
		}

		namespace error
		{
			const char* const name = "error";

			/// <summary>
			/// Possible error codes for <see cref="Arguments.Code"/>
			/// </summary>
			namespace codes
			{
				const int success = 0;
				const int general_error = 100;
				const int general_exception = 101;

				const int license_not_found = 200; // Error.Arguments.Text contains device number
				const int license_key_not_specified = 201;
				const int license_wrong_key_format = 202;
				const int license_wrong_key_type = 203;
				const int license_wrong_key = 204;

				const int coeffs_not_found = 300; // Error.Arguments.Text contains device number
				const int coeffs_mismatch = 301; // Attempt to load coeffs for another device
				const int coeffs_no_measurement_unit = 302; // Can't verify that CB and MU are from same kit, because MU is not found
				const int coeffs_invalid_file_format = 303; // Can't verify that CB and MU are from same kit, because MU is not found

				const int operation_cancelled = 400;
				const int invalid_state = 500;
				const int syntax = 600;

				const int test_device_not_connected = 700;
				const int test_device_invalid_configuration = 701;
			}

			/// <summary>
			/// Possible <see cref="Error"/> command arguments
			/// </summary>
			namespace arguments
			{
				//Detailed error description (if any), Exception text etc
				const char* const text = "error.text";

				//Error code
				const char* const code = "error.code";

				//A piece of source command most closely related to error cause
				const char* const excerpt = "excerpt";
			}
		}

		namespace response
		{
			const char* const name = "response";

			/// <summary>
			/// Possible <see cref="Response"/> command arguments
			/// </summary>
			namespace arguments
			{
				//Original command name from request
				const char* const command_name = "command";
			}
		}

		/// <summary>
		/// 
		/// </summary>
		namespace info
		{
			//Command name
			const char* const name = "info";
			const char* const coreversion = "core.version";
			const char* const hostversion = "host.version";
		}

		/// <summary>
		/// Set localization 
		/// </summary>
		namespace localization
		{
			//Command name
			const char* const name = "localization";

			/// <summary>
			/// Possible <see cref="Cancel"/> command arguments
			/// </summary>
			namespace arguments
			{
				//Tag of task to be cancelled
				const char* const language = "lang";
			}
		}

		/// <summary>
		/// Manage calibration coefficients
		/// </summary>
		namespace coefficients
		{
			//Command name
			const char* const name = "coeffs";

			/// <summary>
			/// Possible <see cref="Coefficients"/> command arguments
			/// </summary>
			namespace arguments
			{
				//Path to file with source coefficients or reserved 'cb' token to load from Control Block
				const char* const source = "src";
				const char* const source_control_block_token = "cb";
				const char* const verify_device_number = "verify";
				//Verify that loaded coefficients number match with connected measurement unit number

				const char* const loaded_kts_device_number = "ndevice";
			}
		}

		/// <summary>
		/// Cancels executing task using 'tag' provided
		/// </summary>
		namespace cancel
		{
			//Command name
			const char* const name = "cancel";

			/// <summary>
			/// Possible <see cref="Cancel"/> command arguments
			/// </summary>
			namespace arguments
			{
				//Tag of task to be cancelled
				const char* const target_tag = "target";
			}
		}

		namespace state
		{
			//Command name
			const char* const name = "state";

			/// <summary>
			/// Possible <see cref="State"/> command arguments
			/// </summary>
			namespace arguments
			{
				const char* const current = "state";
				const char* const next = "state.next";
			}
		}

		/// <summary>
		/// Async update issued by host about something that happened in measurement system
		/// </summary>
		namespace notification
		{
			//Command name
			const char* const name = "notification";

			const char* const status_reserved_token = "!status";

			/// <summary>
			/// Possible <see cref="Notification"/> command arguments
			/// </summary>
			namespace arguments
			{
				/// <summary>
				/// Can be one of <see cref="NotificationEvent.NotificationType"/> or <see cref="StatusReservedToken"/>. 
				/// If it's  <see cref="StatusReservedToken"/>, then <see cref="StatusChangedSourceName"/> argument
				/// contains what has changed, and <see cref="Payload"/> argument contains new value
				/// </summary>
				const char* const notification_type = "type";

				/// <summary>
				/// One of <see cref="StatusTokens"/>
				/// </summary>
				const char* const status_changed_source_name = "source";

				/// <summary>
				/// Data for this notification. 
				/// It may be text content (for <see cref="NotificationEvent.NotificationType.UserLocalizedNotification"/>) 
				/// or some value in string form
				/// </summary>
				const char* const payload = "payload";

				/// <summary>
				/// Some additional data, depending on notification type.
				/// For <see cref="StatusTokens.BatteryChargePercents"/> it may contain <see cref="StatusTokens.BatteryDischargedCondition"/>
				/// if battery is heavily discharged and user is required to charge it immediately
				/// </summary>
				const char* const extra = "extra";
			}

			/// <summary>
			/// 
			/// </summary>
			namespace status_tokens
			{
				/// <summary>
				/// Current measurement system state. Payload - always 
				/// descendant of <see cref="State"/>
				/// </summary>
				const char* const device_complex_current_state = "curstate";


				//Built-in battery charge level in percents
				const char* const battery_charge_percents = "batt";

				/// <summary>
				/// Set in <see cref="Arguments.Extra"/> argument if battery is considered heavily discharged & 
				/// user action required
				/// </summary>
				const char* const battery_discharged_condition = "discharged";
			}
		}

		/// <summary>
		/// Toggles system power on or off
		/// </summary>
		namespace power
		{
			//Command name
			const char* const name = "power";

			/// <summary>
			/// Possible <see cref="Power"/> command arguments
			/// </summary>
			namespace arguments
			{
				const char* const turn_off = "off";
				const char* const turn_on = "on";
			}
		}

		/// <summary>
		/// Add/remove license key
		/// </summary>
		namespace manage_license
		{
			//Command name
			const char* const name = "license";

			const char* const add_action = "add";
			const char* const remove_action = "remove";
			const char* const list_keys_action = "list";

			namespace arguments
			{
				const char* const license_key_value = "key";
				const char* const outcome = "outcome";
			}
		}

		/// <summary>
		/// Configuration action
		/// </summary>
		namespace config
		{
			//Command name
			const char* const name = "config";

			const char* const action = "action";
						
			namespace arguments
			{
				const char* const list_argument = "list";
			}
		}

		/// <summary>
		/// Set test device configuration and read reference values
		/// </summary>
		namespace test_device
		{
			//Command name
			const char* const name = "testdev";

			//What testing configuration to set - either TestConfigurationRx or TestConfigurationCx
			const char* const configuration_argument = "cfg";
			const char* const test_configuration_rx = "rx"; //set test device in Rx testing config
			const char* const test_configuration_cx = "cx"; //set test device in Cx testing config

			//What test configuration to set exactly
			const char* const relay_argument = "relay"; //Set relay number for test device
			const char* const aux_relay_argument = "relay2";
			//Aux relay number for test device (high-tangent config for TestConfigurationCx)

			//Results
			//For Cx combination
			const char* const result_cmin = "cmin"; //Minimum reference capacity in farades
			const char* const result_cmax = "cmax"; //Maximum reference capacity in farades
			const char* const result_tgmin = "tgmin"; //Minimum reference tangent
			const char* const result_tgmax = "tgmax"; //Maximum reference tangent

			//For Rx combination
			const char* const result_rmin = "rmin"; //Minimum reference resistance in ohms
			const char* const result_rmax = "rmax"; //Maximum reference resistance in ohms
		}


		/// <summary>
		/// Starts measurement process
		/// </summary>
		namespace measure
		{
			//Command name
			const char* const name = "measure";

			/// <summary>
			/// Possible <see cref="Measure"/> command arguments
			/// </summary>
			namespace arguments
			{
				/// <summary>
				/// Argument that determines what to measure
				/// </summary>
				const char* const what = "what";
			}

			namespace cx
			{
				/// <summary>
				/// Argument that determines what to measure
				/// </summary>

				const char* const key = "cx";

				const char* const cx_averaging = "avg";
				const char* const c0_value = "c0";
				const char* const tg0_value = "tg0";

				const char* const resulting_capacity = "cx";
				const char* const resulting_tangent = "tgx";
				const char* const resulting_voltage = "ux";
				const char* const resulting_frequency = "fq";
				const char* const sko_cx = "sko_cx";
				const char* const sko_tgx = "sko_tgx";
			}

			namespace ux
			{
				const char* const key = "ux";

				const char* const resulting_voltage = "ux";
				const char* const resulting_frequency = "fq";
			}

			namespace rx
			{
				const char* const key = "rx";
				const char* const megaommeter_voltage = "uset";
				const char* const more_than1_t_ohm_token = ">1t";
				const char* const resulting_rx = "rmeas";
				const char* const resulting_sko_rx = "sko_rmeas";
			}

			namespace ka
			{
				const char* const key = "ka";
				const char* const megaommeter_voltage = "uset";
				const char* const t1 = "t1";
				const char* const t2 = "t2";

				const char* const more_than1_t_ohm_token = ">1t";
				const char* const result_r15 = "r15";
				const char* const result_r60 = "r60";
				const char* const resulting_ka = "ka";
			}
		}
	}
}
